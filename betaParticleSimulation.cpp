#include <iostream>
#include <string>
#include <algorithm>

// Garfield headers
#include "Garfield/MediumMagboltz.hh"
#include "Garfield/SolidBox.hh"
#include "Garfield/GeometrySimple.hh"
#include "Garfield/ComponentConstant.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/AvalancheMicroscopic.hh"

// ROOT headers
#include "TROOT.h"
#include "TH2D.h"
#include "TFile.h"
#include "TTree.h"
#include "TRandom3.h"

using namespace std;
using namespace Garfield;

int main(int argc, char** argv)
{
	// Define the gas medium
	MediumMagboltz gas;
	
	gas.SetComposition("N2", 78.084, "O2", 20.946, "Ar", 0.934, "CO2", 0.036); // Ambient air 
	gas.SetTemperature(293.15); // Gas temperature [K]
	gas.SetPressure(760.); // Gas pressure [Torr] (760 Torr = 1 atm.)
	gas.Initialise(true);
	
	
	// Make the sensor. A gas volume big enough to contain the entire track
	const double gasVolumeSize = 2000.; // [cm]
	SolidBox gasVolume(0., 0., 0., 0.5*gasVolumeSize, 0.5*gasVolumeSize, 0.5*gasVolumeSize); // Cube with sides of gasVolumeSize centred on the origin
	GeometrySimple geometry;
	ComponentConstant component;
	Sensor sensor;
	
	geometry.AddSolid(&gasVolume, &gas);
	component.SetGeometry(&geometry);
	sensor.AddComponent(&component);
	
	
	// Open a ROOT file and create a tree to output the results
	TFile outFile("output.root", "RECREATE");
	TTree eventTree("eventTree", "Events");
	int nIons; // Number of ionisations
	vector<double> ionX, ionY, ionZ; // Ionisation positions
	double endPointX, endPointY, endPointZ; // End point of the beta electron
	
	eventTree.Branch("nIons", &nIons, "nIons/I"); 
	eventTree.Branch("endPointX", &endPointX, "endPointX/D"); 
	eventTree.Branch("endPointY", &endPointY, "endPointY/D"); 
	eventTree.Branch("endPointZ", &endPointZ, "endPointZ/D"); 
	
	TBranch* branchIonX = eventTree.Branch("ionX", NULL, "ionX[nIons]/D"); 
	TBranch* branchIonY = eventTree.Branch("ionY", NULL, "ionY[nIons]/D"); 
	TBranch* branchIonZ = eventTree.Branch("ionZ", NULL, "ionZ[nIons]/D"); 
	
	double plotTransverse     = 200.;
	int    plotTransverseBins = 200 ;
	double plotMinZ  = -150.;
	double plotMaxZ  =  200.;
	int    plotZBins =  350 ;
	TH2D plotEndpoints("plotEndpoints", "Distribution of beta electron endpoints; z [cm]; r [cm]; Bin count [#times 1/r]", 100, plotMinZ, plotMaxZ, 100, 0., plotTransverse);
	TH2D plotIons("plotIons", "Distribution of ionisations; z [cm]; r [cm]; Bin count [#times 1/r]", plotZBins, plotMinZ, plotMaxZ, plotTransverseBins, 0., plotTransverse);
	
	
	// Charge transport object
	AvalancheMicroscopic avaMicro;
	
	avaMicro.SetSensor(&sensor);
	avaMicro.SetElectronTransportCut(10); // In eV
	
	
	// Simulate beta electrons
	double initialEnergy = 0.5e6;  // Electron energy in eV
	const uint64_t nTracks = 100; // Number of betas to simulate
	
	for(uint64_t iTrack=0; iTrack<nTracks; ++iTrack)
	{
		nIons = 0;
		ionX.clear();
		ionY.clear();
		ionZ.clear();
		
		//if(iTrack % 1000 == 0)
			cerr << "\rSimulating event " << iTrack+1 << flush; 
		
		avaMicro.AvalancheElectron(0., 0., 0., 0., initialEnergy, 0., 0., 1.); // Tracks the initial electron and ionisation electrons
		//avaMicro.DriftElectron(0., 0., 0., 0., initialEnergy, 0., 0., 1.); // Only tracks the initial electron
		
		
		const int nElectronEndpoints = avaMicro.GetNumberOfElectronEndpoints();
		double x0, y0, z0, t0, e0, x1, y1, z1, t1, e1;
		int status;
		
		for(int iElectron=0; iElectron<nElectronEndpoints; ++iElectron)
		{
			avaMicro.GetElectronEndpoint(iElectron, x0, y0, z0, t0, e0, x1, y1, z1, t1, e1, status);
			
			if(x0 != 0. || y0 != 0. || z0 != 0. || e0 != initialEnergy) // This is an ionisation electron
			{
				ionX.push_back(x0);
				ionY.push_back(y0);
				ionZ.push_back(z0);
				++nIons;
				
				
				double r = sqrt(x0*x0 + y0*y0);
				
				if(r != 0.) plotIons.Fill(z0, r, 1./r); // Make a plot of the radial distribution
			}
			else // This is the initial electron
			{
				endPointX = x1;
				endPointY = y1;
				endPointZ = z1;
				
				
				double r = sqrt(x1*x1 + y1*y1);
				
				if(r != 0.) plotEndpoints.Fill(z1, r, 1./r); // Make a plot of the radial distribution
			}
		}
		
		if(nIons)
		{
			branchIonX->SetAddress(&ionX[0]);
			branchIonY->SetAddress(&ionY[0]);
			branchIonZ->SetAddress(&ionZ[0]);
		}
		
		eventTree.Fill();
	}
	
	cerr << endl;
	
	ionX.clear();
	ionY.clear();
	ionZ.clear();
	
	
	// Close the output file
	eventTree.AutoSave();
	outFile.Write(0, TObject::kOverwrite);
	outFile.Close();
	
	return 0;
}















































































