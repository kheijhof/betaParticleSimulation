LINK.o = $(LINK.cc)
CXXFLAGS = -std=c++11 -O3 -Wall -Wfatal-errors -m64 `root-config --cflags` -fno-common -I$(GARFIELD_HOME)/Include -I$(GARFIELD_HOME)/Heed 
LDFLAGS = -L$(GARFIELD_HOME)/Library -L`root-config --libdir`
LDLIBS =  -lGarfield  `root-config --noldflags --glibs` -lTreePlayer -lMinuit -lGeom -lm  -lgfortran

.PHONY: clean

betaParticleSimulation: betaParticleSimulation.cpp

clean:
	$(RM) betaParticleSimulation
